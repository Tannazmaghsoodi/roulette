import java.util.Random;

public class RouletteWheel {
    private Random random;
    private int spinNumber;

    public RouletteWheel() {
        this.random = new Random();
        this.spinNumber = 0;
    }

    public void spin() {
        this.spinNumber = random.nextInt(37);
    }

    public int getValue() {
        return this.spinNumber;
    }
}
