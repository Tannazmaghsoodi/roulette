import java.util.Scanner;

public class Roulette{
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        RouletteWheel theWheel = new RouletteWheel();
        int playerInitialBalance = 1000;
        
        System.out.println("Hi, welcome to Roullete game.");
        System.out.println("You have $" + playerInitialBalance);

        while (playerInitialBalance > 0) {
            
            System.out.print("If you are willing to place a bet type Yes or No ");
            String playerResponse = keyboard.next().toLowerCase();
            while (!playerResponse.equals("yes") && !playerResponse.equals("no")) {
                System.out.println("Invalid input. Please type Yes or No.");
                System.out.print("If you are willing to place a bet, type Yes or No: ");
                playerResponse = keyboard.nextLine().toLowerCase();
            }
            if (playerResponse.equals("no")) {
                break;
            }

            int betAmount = 0;
            boolean betAmoutValidation = false;
            while (!betAmoutValidation) {
                System.out.print("Enter the amount you want to bet: ");
                if (keyboard.hasNextInt()){
                    betAmount = keyboard.nextInt();
                    if (betAmount > playerInitialBalance){
                        System.out.println("Sorry. You cannot place a bet because you do not have enough money");
                    }else if(betAmount <= 0){
                        System.out.println("Input a positive number please. Your input is invalid");
                    }else{
                        betAmoutValidation = true;
                    }
                }else {
                    System.out.println("Your input should be an a number between 1 and 1000. Not a letter or word. Let's try again");
                    keyboard.next();
                }
            }

    
            int betNumber = -1;
            boolean betNumberValidation = false;
            while (!betNumberValidation) {
                System.out.print("Enter the number you want to bet on (0-36): ");
                if (keyboard.hasNextInt()){
                    betNumber = keyboard.nextInt();
                    if (betNumber < 0 || betNumber > 36) {
                        System.out.println("Your input should be an a number between 0 and 36. Try again");
                    }else{
                        betNumberValidation = true;
                    }
                } else {
                    System.out.println("Your input should be an a number between 0 and 36. Not a letter or word. Let's try again");
                    keyboard.nextLine();
                }
            }
            keyboard.nextLine();

            theWheel.spin();
            int spinResult = theWheel.getValue();
            System.out.println("The wheel spun and the number is " + spinResult);
            if (spinResult == betNumber) {
                int winnings = betAmount * 35;
                playerInitialBalance += winnings;
                System.out.println("Congratulations! You won $" + winnings);
            } else {
                playerInitialBalance -= betAmount;
                System.out.println("Sorry, you lost $" + betAmount);
            }
        }
        System.out.println("Thanks for playing. The game is over. You finished with $" + playerInitialBalance);
    }
    
}

